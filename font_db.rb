#!/usr/bin/ruby

# == Synopsis
# Create and access a database of fonts.
#
# == Usage
# ruby font_db.rb
#
# ==Author
# Tim Dresser

require 'singleton'
require 'sqlite3'
require 'font.rb'

# Create and access a database of fonts.
# The instance is accessed using FontDB.instance()
#
class FontDB
  # Ensures that only one can exist.
  include Singleton

  # Constants for use in a few functions.

  # FIXME: 
  # Where should they go?
  UP = true
  DOWN = false
  NO_LIMIT = -1

  # FIXME: 
  # Get this stuff from a textfile or something.
  FONT_DIRECTORIES = ['/usr/share/fonts','C:\WINDOWS\Fonts']
  FONT_TYPES = ["ttf","otf"]
  
  # Add these tags based on font metadata.
  AUTO_TAGS = %w(bold serif italic sans-serif 
                sans\ serif hairline modern slab\ serif script)

  # The associated data cannot coexist. The right side is removed.
  AUTO_TAGS_EXCLUSIVE = {"sans-serif" => "serif", "sans serif" => "serif"}

  # The associated data should be combined. Left side becomes right side.
  AUTO_TAGS_COMBINE = {"sans-serif" => "sans serif"}

  # Columns to search through for tags.
  AUTO_TAGS_COLUMNS= %w(family subfamily name description)

  def initialize()
    @db = SQLite3::Database.new('database.db')
  end

  # Gets data out of the database.
  # For example
  #
  # FontDB.instance.get("name","id",DOWN, 5, 10)
  #
  # will display the names, sorted by decreasing ID starting at #10, and 
  # going to number 15
  #
  def get(columns, order_by='name', sort_dir = UP, limit = NO_LIMIT, offset = 0)
    if sort_dir = UP then
      dir = "ASC"
    else
      dir = "DESC"
    end
    return @db.execute(%{
      SELECT #{[columns].flatten.join(", ")} 
      FROM fonts 
      ORDER BY #{order_by} #{dir} 
      LIMIT #{limit} 
      OFFSET #{offset}
    })
  end

  # Creates the required tables.
  #
  def create_tables()
    if !FontDB.instance.tables_exist? then
      @db.execute_batch(%{
        CREATE TABLE fonts 
          (
            id              INTEGER PRIMARY KEY,
            path            VARCHAR(255) UNIQUE ON CONFLICT IGNORE,
            rating          INTEGER,
            copyright       VARCHAR(2048),
            family          VARCHAR(255),
            subfamily       VARCHAR(255),
            name            VARCHAR(255),
            foundry         VARCHAR(255),
            description     VARCHAR(2048),
            license         VARCHAR(255),
            defaulttext     VARCHAR(255)
          );

	CREATE TABLE tags
          (
            id            INTEGER PRIMARY KEY,
            name      VARCHAR(255) UNIQUE ON CONFLICT IGNORE
          );

        CREATE TABLE tags_fonts
          (
            tag_id        INTEGER,
            font_id       INTEGER,
            PRIMARY KEY (tag_id, font_id) ON CONFLICT IGNORE
            
          );

	CREATE TABLE sets
          (
            id            INTEGER PRIMARY KEY,
            name      VARCHAR(255) UNIQUE ON CONFLICT IGNORE
          );

        CREATE TABLE sets_fonts
          (
            set_id INTEGER,
            font_id INTEGER,
            PRIMARY KEY (set_id, font_id) ON CONFLICT IGNORE
          );
        })
      return true
    else
      return false
    end
  end

  # Removes all tables from the database.
  #
  def drop_tables()
    if FontDB.instance.tables_exist? then
      @db.execute_batch(%{
        DROP TABLE fonts;
	DROP TABLE tags;
	DROP TABLE tags_fonts;
	DROP TABLE sets;
	DROP TABLE sets_fonts;
      })
      return true
    else
      return false
    end
  end

  # Adds a font to the database.
  # FIXME:
  # This should auto_tag_font.
  #
  def add_font(path)
    font_data = [path] + Font.meta(path)
    @db.execute(%{
      INSERT INTO fonts 
      (
        path, copyright, family, subfamily,
        name, foundry, description, license, defaulttext
      ) 
      VALUES (
        '#{font_data.collect{|x|x.to_s}.join(%{','})}'
      )
    })
  end

  # Populates the table based on system directories.
  #
  def populate()
    Font.find(FONT_DIRECTORIES, FONT_TYPES).each do |path|
      add_font(path)
    end
  end
 
  # FIXME:: add in sets.
  #
  def to_s
    return @db.execute(%{
      SELECT * FROM fonts
    }).collect do |meta|
       ("-------")+"\n" + meta.join("\n") + "\n" + read_tags(meta[0]).join(", ")
    end.join("\n")
  end

  # Has create_tables been called?
  #
  def tables_exist?
    return !@db.execute(%{
      SELECT name FROM sqlite_master WHERE type = 'table'
    }).empty?
  end
  
  private

  # A set of functions for acting on sets or tags.
  
  # Add an array of groups to an array of font ids.
  #
  def add_groups(font_ids, groups, type)
    [groups].flatten.each do |group|
      # Add the group to the global table of groups.
      @db.execute(%{
        INSERT INTO #{type}s (name)
        VALUES ('#{group}')
      })
      [font_ids].flatten.each do |font_id|
        # Add an association between the font and the group.
        @db.execute(%{
          INSERT INTO #{type}s_fonts 
          (
            #{type}_id, 
            font_id
          )
          VALUES 
          (
            (SELECT id FROM #{type}s WHERE name = '#{group}'),
            #{font_id}
          )
        })
      end
    end
  end

  # Removes groups from the given fonts.
  #
  def remove_groups(font_ids, groups, type)
    [groups].flatten.each do |group|
      [font_ids].flatten.each do |font_id|
        @db.execute(%{
          DELETE FROM #{type}s_fonts
          WHERE font_id = '#{font_id}'
          AND #{type}_id = 
          (
            SELECT id FROM #{type}s  
            WHERE name = '#{group}'
          )
        })
      end
    end
  end 

  # Combines groups named old_names into one named new_name.
  #
  def combine_groups(new_name, old_names, type)
    [old_names].flatten.each do |old_name|
      ids = tag_members(old_name)      
      remove_tags(ids, old_names)
      add_tags(ids, new_name)
    end
  end

  # Returns all groups associated with a font ID.
  #
  def read_groups(font_id, type)
    return @db.execute(%{
      SELECT name FROM #{type}s
      INNER JOIN #{type}s_fonts
      ON #{type}s.id = #{type}s_fonts.tag_id
      WHERE #{type}s_fonts.font_id = '#{font_id}'
    })
  end

  # Returns the font IDs of all members of group.
  #
  def group_members(group, type)
    return @db.execute(%{
      SELECT font_id FROM #{type}s_fonts
      INNER JOIN #{type}s
      ON #{type}s_fonts.tag_id = #{type}s.id
      WHERE #{type}s.name = '#{group}'
    })
  end
  
  # Returns a list of all groups.
  #
  def groups(type)
    return @db.execute(%{
      SELECT name FROM #{type}s
    })
  end

  public 

  # Adds the tags to the given fonts.
  #
  def add_tags(font_ids, tags)
    add_groups(font_ids, tags, "tag")
  end

  # Adds the sets to the given fonts.
  #
  def add_sets(font_ids, set)
    add_groups(font_ids, tags, "set")
  end

  # Removes the tags from the given fonts.
  #
  def remove_tags(font_ids, tags)
    remove_groups(font_ids, tags, "tag")
  end

  # Removes the sets from the given fonts.
  #
  def remove_sets(font_ids, tags)
    remove_groups(font_ids, tags, "set")
  end
  
  # Changes all old_tags to new_tag.
  #
  def combine_tags(new_tag, *old_tags)
    combine_groups(new_tag, old_tags, "tag")
  end
  
  # Changes all old_sets to new_set.
  #
  def combine_sets(new_set, *old_sets)
    combine_groups(new_set, old_sets, "set")
  end

  # Lists all of the given font's tags.
  #
  def read_tags(font_id)
    read_groups(font_id, "tag")
  end

  # Lists all of the given font's sets.
  #
  def read_sets(font_id)
    read_groups(font_id, "set")
  end

  # Lists all of the tag's members.
  #
  def tag_members(tag_name)
    group_members(tag_name, "tag")
  end
  
  # Lists all of the set's members.
  #
  def set_members(set_name)
    group_members(set_name, "set")
  end
  
  # Lists all tags.
  def tags()
    groups("tag")
  end

  # Lists all sets.
  def sets()
    groups("set")
  end

  # Automatically tags all fonts.
  # FIXME: should constants be in here?
  #
  def auto_tag(auto_tags, auto_tags_exclusive, 
                    auto_tags_combine, auto_tags_search_columns)
    auto_tags.each do |tag|
      # Search through all columns given for tags in the auto_tag list.
      all_columns = auto_tags_search_columns.join(" LIKE '%#{tag}%' OR ")
      ids = @db.execute(%{
        SELECT id FROM fonts
        WHERE #{all_columns} 
        LIKE '%#{tag}%'
      })
      add_tags(ids, tag)
    end
    
    auto_tags_combine.each_key do |tag_keep|
      combine_tags(tag_keep, auto_tags_combine[tag_keep])
    end
    
    auto_tags_exclusive.each_key do |tag_keep|
      ids = tag_members(tag_keep) & tag_members(auto_tags_exclusive[tag_keep])
      remove_tags(ids, (auto_tags_exclusive[tag_keep]))
    end
  end

  # auto_tag one font.
  #
  def auto_tag_font(id, auto_tags, auto_tags_exclusive, 
                    auto_tags_combine, auto_tags_search_columns)
    tags=[]
    auto_tags.each do |tag|
      # Search through all columns given for tags in the auto_tag list
      all_columns = auto_tags_search_columns.join(" LIKE '%#{tag}%' OR ")
      if !@db.execute(%{
        SELECT id FROM fonts 
        WHERE (id = #{id}) 
        AND (#{all_columns} LIKE '%#{tag}%')
      }).empty? then
        tags.push(tag)
      end
      add_tags(id, tags)
    end
   
    # FIXME:
    # This shouldn't be global. 
    auto_tags_combine.each_key do |tag_keep|
      combine_tags(tag_keep, auto_tags_combine[tag_keep])
    end
    
    auto_tags_exclusive.each_key do |tag_keep|
      ids = tag_members(tag_keep) & tag_members(auto_tags_exclusive[tag_keep])
      remove_tags(ids, (auto_tags_exclusive[tag_keep]))
    end
  end
end

db = FontDB.instance
# FIXME:
# Should be a transaction to do all starting stuff.
db.create_tables
# FIXME? 
# Should it do a full scan every time?
db.populate
db.auto_tag(FontDB::AUTO_TAGS, FontDB::AUTO_TAGS_EXCLUSIVE, 
                      FontDB::AUTO_TAGS_COMBINE, FontDB::AUTO_TAGS_COLUMNS)
puts db
