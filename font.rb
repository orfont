#!/usr/bin/ruby

# == Synopsis
# Handles finding and reading font files.
#
# == Usage
# No commandline usage.
#
# ==Author
# Tim Dresser


require 'ttf'

#Handles finding and reading font files.
#
class Font
  #Recursively searches through a directory for fonts.
  #
  def Font.find_in_dir(directory, extensions)
    paths = []
    if File.directory?(directory) then
      Dir.glob("#{directory}/*") do |file|
        next if file[0] == ?. 
        if File.directory? file 
          paths.push(*Font.find_in_dir(file, extensions))
        elsif font?(file, extensions)
          paths.push(file)
        end
      end
    end
    return paths
  end
  
  #Recursively searches through an array of directories for fonts.
  #
  def Font.find(directories, extensions)
    paths = []
    directories.each do |directory|
      paths.push(Font.find_in_dir(directory, extensions))
    end
    return paths.flatten
  end

  #Get metadata from a TTF file.
  #Indexes used are:
  #
  # * 0 Copyright 
  # * 1 Family 
  # * 2 Subfamily 
  # * 4 Full Name 
  # * 8 Foundry 
  # * 10 Description 
  # * 14 License URL 
  # * 19 Default Text 
  #
  def Font.meta(path)
        #FIXME 
        #Stop using this library, its slow and bulky for what needs to be done.
        font = FontTTF::TTF::File.new(path)
        meta = font.get_table(:name).name_records.values_at(0,1,2,4,8,10,14,19)
        meta.collect!{|meta_item|
	  #Clean up metadata
	  
          #Escape nasty characters, get rid of quotation marks around data,
	  #replace ' with '', remove trailing and leading whitespace,
 	  #get rid of successive spaces
          meta_item.to_s.dump.gsub("\\000","")[1..-2].gsub("'","''") \
	    .gsub(/^\s+/, "").gsub(/\s+$/, $/).gsub(/ +/," ")
        }
        return meta
  end
  
  private

    #Returns true if a path has one of the given extensions.
    #
    def Font.font?(path, extensions)
      extensions.each do |extension|
	if path[-extension.length..-1] == extension then
	  return true
	end
      end
      return false
    end
end
